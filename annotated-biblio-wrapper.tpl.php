<?php
/**
 * @file annotated-biblio-wrapper.tpl.php
 * Handles content surrounding the entries. The following variables is
 * available:
 *   - $biblio_content The rendered bibliographic entries.
 *   - $biblio_count The number of bibliographic entries.
 *
 */
?>
<div class="annotated-bibliography">
<h3><?php print t('References'); ?></h3>
<?php print $biblio_content; ?>
</div>
