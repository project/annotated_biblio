<?php
/**
 * @file annotated-biblio.tpl.php
 * Handles rendering the annotated bibliography for a node. The following
 * variables are available:
 *   - $aid The unique ID of the annotation.
 *   - $annotation The filtered text of the annotation.
 *   - $biblio The rendered Biblio entry.
 *   - $links Present only if the user has 'edit bibliography' permissions. It
 *     contains a rendered list links for editing and deleting the annotation.
 *   - $zebra Contains "even" for even items and "odd" for odd items.
 *
 */

?>
<div class="annotated-entry <?php print $zebra; ?>">
  <div class="annotated-biblio"><?php print $biblio;?></div>
  <?php if (!empty($annotation)): ?>
    <div class="annotated-text"><?php print $annotation; ?></div>
  <?php endif; ?>
  <?php if (isset($links)): ?>
    <div class="meta"><?php print $links; ?></div>
  <?php endif; ?>
</div>
